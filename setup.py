from setuptools import setup

setup(name='graphqlclient',
      version='0.0.1',
      author='koodir',
      author_email='koodir@gmx.com',
      packages=['gqlc'],
      license='MIT',
      description='Simple GraphQL Client',
      long_description=open('README.md').read(),
      install_requires=[
          'certifi==2017.4.17',
          'chardet==3.0.4',
          'idna==2.5',
          'requests',
          'urllib3==1.21.1'
      ],
      zip_safe=False)
