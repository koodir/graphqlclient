#!/usr/bin/env python

from gqlc import GraphQLClient


def main():
    client = GraphQLClient('https://swapi.apis.guru/')
    result = client.execute('''
 query{
  allFilms {
    films {
      id
      title
    }
  }
}

''')
    print(result)
    print(result.text)


if __name__ == '__main__':
    main()
