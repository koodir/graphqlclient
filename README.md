# python-graphql-client
Simple GraphQL client for Python

## Install

#### Virtualenv (optional/recommended)

- Create a directory and enter

`mkdir MyProject`
`cd MyProject`

- Init a virtualenv

`virtualenv -p python3 venv`

- Activate the virtual environment

`source venv/bin/activate`

Howto install `virtualenv` on your system can you find [here](https://virtualenv.pypa.io/en/stable/installation/)

- Once activated the `graphQLClient` package can be installed with:

`pip install -e git+https://koodir@bitbucket.org/koodir/graphqlclient.git#egg=requirements.txt`

## Usage


```py
from gqlc import GraphQLClient

client = GraphQLClient('http://graphql-swapi.parseapp.com/')

result = client.execute('''
{
  allFilms {
    films {
      title
    }
  }
}
''')

print(result)
```

## License

[MIT License](http://opensource.org/licenses/MIT)
