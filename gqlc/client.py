import json
import requests

SPACE = ' '


class GraphQLClient:
    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.token = None
        self.header = None
        self.header_type = None

    def read(self, filename):
        with open(filename, 'r') as fp:
            query = fp.read()
        return query

    def execute(self, query, variables=None):
        return self._send(query, variables)

    def inject_token(self, token, header, header_type):
        self.token = token
        self.header = header
        self.header_type = header_type

    def _set_header(self, headers):
        value = ''
        if(self.header_type):
            value += self.header_type + SPACE
        if(self.token):
            value += self.token
        if(self.header):
            headers[self.header] = value

    def _send(self, query, variables):
        data = {'query': query,
                'variables': variables}
        headers = {'Accept': 'application/json',
                   'Content-Type': 'application/json'}
        if self.token is not None:
            self._set_header(headers)

        try:
            return requests.post(self.endpoint,  data=json.dumps(data).encode('utf-8'), headers=headers)
        except requests.exceptions.HTTPError as e:
            print(e.read())
            print('')
            raise e
